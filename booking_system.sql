-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2020 at 10:29 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `booking_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `baak`
--

CREATE TABLE `baak` (
  `id_baak` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `baak`
--

INSERT INTO `baak` (`id_baak`, `email`, `pwd`) VALUES
(1, 'baak@gmail', 'baak01');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kls` int(11) NOT NULL,
  `ruangan` varchar(50) NOT NULL,
  `status_kelas` varchar(50) NOT NULL,
  `jamBooking` time NOT NULL,
  `jamSelesai` time NOT NULL,
  `id_baak` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mhs`, `nama`, `email`, `prodi`, `password`) VALUES
(1, 'Winda Sinurat', 'if318051', 'D3 Teknologi Informasi', 'winda01'),
(2, 'Lestari Uli Lumban Gaol', 'if318054', 'D3 Teknologi Informasi', 'lestari01');

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_booking`
--

CREATE TABLE `riwayat_booking` (
  `idBooking` int(11) NOT NULL,
  `tglBooking` date NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `id_kls` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `role`) VALUES
('baak01', 'baak', 'baak'),
('if318001', 'Palti', 'Mahasiswa'),
('if318054', 'lestari21', 'Mahasiswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baak`
--
ALTER TABLE `baak`
  ADD PRIMARY KEY (`id_baak`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kls`),
  ADD KEY `id_baak` (`id_baak`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`);

--
-- Indexes for table `riwayat_booking`
--
ALTER TABLE `riwayat_booking`
  ADD PRIMARY KEY (`idBooking`),
  ADD KEY `id_kls` (`id_kls`),
  ADD KEY `riwayat_booking_ibfk_1` (`id_mhs`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`id_baak`) REFERENCES `baak` (`id_baak`);

--
-- Constraints for table `riwayat_booking`
--
ALTER TABLE `riwayat_booking`
  ADD CONSTRAINT `riwayat_booking_ibfk_1` FOREIGN KEY (`id_mhs`) REFERENCES `mahasiswa` (`id_mhs`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
