/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookingguirmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface jdbcinterface extends Remote{
   public String Insert(int id_kls,String ruangan, String status_kelas, String jam_booking, int id_baak,String jam_selesai) throws RemoteException;
   public String Delete(int id_kls) throws RemoteException;
   public String Update(int id_kls,String ruangan, String status_kelas, String jam_booking, int id_baak,String jam_selesai) throws RemoteException;
   public ArrayList search(int id_kls) throws RemoteException;
}
