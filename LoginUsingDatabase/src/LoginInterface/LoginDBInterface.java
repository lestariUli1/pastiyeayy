/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoginInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author YUDHA
 */
public interface LoginDBInterface extends Remote {
    public boolean setString(String username, String password, String usertype) throws RemoteException;
    
}
